const path = require('path');
const express = require("express");
const app = express();
const cors = require('cors');
const SocketIO = require('socket.io');
const sqlite3 = require('sqlite3').verbose();
const ip = require('ip');
app.use(express.json());
app.use(cors({ origin: true }));
app.use(express.urlencoded());

let itemsQualify = {
    item1: "composicion_artistica",
    item2: "efectos_visuales",
    item3: "completacion_musical",
    item4: "nivel_dificultad",
    item5: "control_fuerza",
    item6: "ejecucion_adecuada",
    item7: "sincronizacion_uniformidad",
    item8: "proyeccion_presencia",
    item9: "desplazamiento_escenario",
    item10: "vestuario",
    item11: "entretenimiento",
    item12: "conexion_publico",
}
let db = new sqlite3.Database('./bnf.db', (err) => {
    if (err) {
        console.log('error when creating the database', err);
    }
    else {
        console.log('Database created');

        //createTable();
    }

});

function addDataToSqLite(items) {
    //create table if not exist
    db.get(`SELECT name from sqlite_master WHERE type='table' AND name='${items.category}'`, (error, row) => {
        if (row !== undefined) {
            let sql = `SELECT competitor_name FROM ${items.category} WHERE competitor_name='${items.competitor_name}'`;
            db.all(sql, [], (err, rows) => {
                if (rows.length > 0) {
                    console.log("updating data");
                    if (items.judge === "juez1") {
                        db.run(`UPDATE ${items.category} SET ${itemsQualify.item1}='${items.item1}', ${itemsQualify.item2}=${items.item2},${itemsQualify.item3}=${items.item3},${itemsQualify.item4}=${items.item4} WHERE competitor_name = '${items.competitor_name}'`);
                        console.log("segundo");
                        return true;
                    }
                    else if (items.judge === "juez2") {
                        db.run(`UPDATE ${items.category} SET ${itemsQualify.item5}=${items.item1}, ${itemsQualify.item6}=${items.item2},${itemsQualify.item7}=${items.item3},${itemsQualify.item8}=${items.item4} WHERE competitor_name = '${items.competitor_name}'`);
                    }
                    else if (items.judge === "juez3") {
                        db.run(`UPDATE ${items.category} SET ${itemsQualify.item9}=${items.item1}, ${itemsQualify.item10}=${items.item2},${itemsQualify.item11}=${items.item3},${itemsQualify.item12}=${items.item4} WHERE competitor_name = '${items.competitor_name}'`);
                    }
                }
                else {
                    console.log("No hay registro de usuario - creando");
                    if (items.judge === "juez1") {
                        db.run(`INSERT INTO ${items.category}(competitor_name,${itemsQualify.item1},${itemsQualify.item2},${itemsQualify.item3},${itemsQualify.item4}) VALUES(?,?,?,?,?)`, [`${items.competitor_name}`, `${items.item1}`, `${items.item2}`, `${items.item3}`, `${items.item4}`]);
                        return;
                    }
                    else if (items.judge === "juez2") {
                        db.run(`INSERT INTO ${items.category}(competitor_name,${itemsQualify.item5},${itemsQualify.item6},${itemsQualify.item7},${itemsQualify.item8}) VALUES(?,?,?,?,?)`, [`${items.competitor_name}`, `${items.item5}`, `${items.item6}`, `${items.item7}`, `${items.item8}`]);
                        return;
                    }
                    else if (items.judge === "juez3") {
                        db.run(`INSERT INTO ${items.category}(competitor_name,${itemsQualify.item9},${itemsQualify.item10},${itemsQualify.item11},${itemsQualify.item12}) VALUES(?,?,?,?,?)`, [`${items.competitor_name}`, `${items.item9}`, `${items.item10}`, `${items.item11}`, `${items.item12}`]);
                        return;
                    }
                }
            });
        }
        else {
            console.log("Creando Tabla");
            db.serialize(() => {
                db.run(`CREATE TABLE ${items.category} (id INTEGER PRIMARY KEY AUTOINCREMENT, competitor_name text,${itemsQualify.item1} text,${itemsQualify.item2} text,${itemsQualify.item3} text,${itemsQualify.item4} text,${itemsQualify.item5} text,${itemsQualify.item6} text,${itemsQualify.item7} text,${itemsQualify.item8} text,${itemsQualify.item9} text,${itemsQualify.item10} text,${itemsQualify.item11} text,${itemsQualify.item12} text)`, (error) => {
                    console.log("error " + error);
                });
                if (items.judge === "juez1") {
                    db.run(`INSERT INTO ${items.category}(competitor_name,${itemsQualify.item1},${itemsQualify.item2},${itemsQualify.item3},${itemsQualify.item4}) VALUES(?,?,?,?,?)`, [`${items.competitor_name}`, `${items.item1}`, `${items.item2}`, `${items.item3}`, `${items.item4}`]);
                    return;
                }
                else if (items.judge === "juez2") {
                    db.run(`INSERT INTO ${items.category}(competitor_name,${itemsQualify.item5},${itemsQualify.item6},${itemsQualify.item7},${itemsQualify.item8}) VALUES(?,?,?,?,?)`, [`${items.competitor_name}`, `${items.item1}`, `${items.item2}`, `${items.item3}`, `${items.item4}`]);
                    return;
                }
                else if (items.judge === "juez3") {
                    db.run(`INSERT INTO ${items.category}(competitor_name,${itemsQualify.item9},${itemsQualify.item10},${itemsQualify.item11},${itemsQualify.item12}) VALUES(?,?,?,?,?)`, [`${items.competitor_name}`, `${items.item1}`, `${items.item2}`, `${items.item3}`, `${items.item4}`]);
                    return;
                }
            });

        }
    });
}
//settings
app.set('port', process.env.PORT || 3000);

app.post('/add-ratings', (req, res) => {

    addDataToSqLite(req.body);
    console.log(req.body);
    res.status(200).json({ response: "ok" });

});

//Static files
app.use(express.static(path.join(__dirname, 'public')));

const server = app.listen(app.get('port'), () => {
    console.log("Server IP: " + ip.address() + ":"  + app.get('port'));
});

//webSockets
const io = SocketIO.listen(server);

io.on('connection', (socket) => {
    console.log("new conection: " + socket.id);
    socket.on('chat:message', (data) => {
        io.sockets.emit('chat:message', data);
    })
});